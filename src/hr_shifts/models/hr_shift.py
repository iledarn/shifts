from datetime import MAXYEAR, timedelta

from odoo import _, api, fields, models  # noqa


class VelocarrierArea(models.Model):
    _name = "shift.area"

    name = fields.Char(string="Area", required=True)
    parent_id = fields.Many2one("shift.area", string="Parent Area", index=True)


class VelocarrierShift(models.Model):
    _name = "hr.shift"

    name = fields.Char()

    area_id = fields.Many2one("shift.area", string="Area")
    employee_ids = fields.Many2many("hr.employee", string="Employees")
    start = fields.Datetime("Start", required=True)
    stop = fields.Datetime("Stop", required=True)
    duration = fields.Float("Duration")

    @api.onchange("start", "duration")
    def _onchange_duration(self):
        if self.start:
            self.stop = (
                self.start + timedelta(hours=self.duration) - timedelta(seconds=1)
            )
